from abc import abstractmethod


class AnnotationParameters:
    def __init__(self, text, xy_data, xy_text=None, arrowprops=None, annotationclip=None, textkwargs=None):
        self.text = text
        self.xy_data = xy_data
        self.xy_text = xy_text
        if arrowprops is None:
            arrowprops = {}
        self.arrowprops = arrowprops
        self.annotationclip = annotationclip
        if textkwargs is None:
            textkwargs = {}
        self.textkwargs = textkwargs
        self.parameters.update(textkwargs)

    @property
    def parameters(self):
        parameters = {'text': self.text,
                      'xy': self.xy_data,
                      'xytext': self.xy_text,
                      'arrowprops': self.arrowprops,
                      'annotation_clip': self.annotationclip}
        parameters.update(self.textkwargs)
        return parameters


class CursorAnnotator:
    def __init__(self, canvas):
        """

        :param backend_gtk3.FigureCanvasGTK3Agg canvas:
        """
        self.canvas = canvas
        self.annotations = []
        ":type: list[matplotlib.artist.Artist]"
        self.canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
        self.canvas.mpl_connect('pick_event', self.on_pick_event)

    @property
    def figure(self):
        return self.canvas

    def remove_annotation(self):
        for annotation in self.annotations:
            self.canvas.remove_overlay_artist(annotation)
        self.annotations = []

    @abstractmethod
    def make_annotation(self, pick_event):
        """ Returns the parameters to build the annotation artist.

        :param pick_event:
        :rtype: AnnotationParameters
        :return:
        """
        raise NotImplementedError()

    def on_mouse_move(self, mouseevent):
        self.remove_annotation()
        x_data = mouseevent.xdata
        y_data = mouseevent.ydata
        if x_data is not None and y_data is not None:
            self.figure.figure.pick(mouseevent)
        self.canvas.redraw_overlay_artists()

    def on_pick_event(self, pick_event):
        annotation_parameters = self.make_annotation(pick_event)
        if annotation_parameters is not None:
            self.position_text_annotation(pick_event, annotation_parameters)
            annotation = pick_event.mouseevent.inaxes.annotate(**annotation_parameters.parameters)
            self.annotations.append(annotation)
            self.canvas.add_overlay_artist(annotation)

    def position_text_annotation(self, pick_event, annotation_parameters):
        """

        :param pick_event:
        :param AnnotationParameters annotation_parameters:
        :return:
        """
        delta = 10
        ax = pick_event.mouseevent.inaxes
        x, y = ax.transData.transform(annotation_parameters.xy_data)
        x_axes, y_axes = ax.transAxes.inverted().transform((x, y))
        if x_axes < 0.5:
            x_offset = delta
            h_alignment = 'left'
        else:
            x_offset = -delta
            h_alignment = 'right'
        if y_axes > 0.5:
            y_offset = -delta
            v_alignment = 'top'
        else:
            y_offset = delta
            v_alignment = 'bottom'
        annotation_parameters.xy_text = ax.transData.inverted().transform((x + x_offset, y + y_offset))
        annotation_parameters.textkwargs['horizontalalignment'] = h_alignment
        annotation_parameters.textkwargs['verticalalignment'] = v_alignment
