import logging
from typing import Tuple, Optional, List, Callable, Any, Dict

import numpy as np
import time
from threading import Thread

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")

from gi.repository import Gdk, GdkPixbuf, GObject, GLib, Gtk
import matplotlib
from matplotlib.artist import Artist
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.backends.backend_gtk3 import FigureCanvasGTK3
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar2GTK3_old

logger = logging.getLogger(__name__)


if matplotlib.__version__ < '3.5':
    from matplotlib.backends.backend_gtk3 import cursord
else:
    from matplotlib.backends.backend_gtk3 import _mpl_to_gtk_cursor

    class _CursorD:
        def __getitem__(self, item):
            return _mpl_to_gtk_cursor(item)

    cursord = _CursorD()

START_RENDERING_FIGURE_SIGNAL = 'start_rendering_figure'
"""Signal sent before starting the figure rendering.
callback(widget, user_request, data)
user_request is True if the redraw results from a user action.
                False if the redraw originates from the interface like resizing
Connecting to this signal allows changing the figure content before rendering
"""

START_RENDERING_OVERLAY_SIGNAL = 'start_rendering_overlay'
"""Signal sent before starting the rendering of overlay, the raw plot has already been rendered.
callback(widget, user_request, data)
user_request is True if the redraw results from a user action.
                False if the redraw originates from the interface like resizing
Connecting to this signal allows changing the overlay content before rendering
"""

END_RENDERING_FIGURE_SIGNAL = 'end_rendering_figure'
"""Signal sent when figure has been rendered.
callback(widget, user_request, data)
user_request is True if the redraw results from a user action.
                False if the redraw originates from the interface like resizing
Connect to this signal to cache the drawing before adding artists to the figure.
"""


class Rubberband:
    def __init__(self, x0=0, y0=0, x1=0, y1=0, visible=False):
        self.visible = visible
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def update(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def draw_rubberband(self, canvas, ctx=None):
        if not self.visible:
            return
        'adapted from http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/189744'
        if ctx is None:
            ctx = canvas.get_property("window").cairo_create()

        height = canvas.figure.bbox.height
        y1 = height - self.y1
        y0 = height - self.y0
        w = abs(self.x1 - self.x0)
        h = abs(y1 - y0)
        rect = [int(val) for val in (min(self.x0, self.x1), min(y0, y1), w, h)]

        ctx.new_path()
        ctx.set_line_width(0.5)
        ctx.rectangle(rect[0], rect[1], rect[2], rect[3])
        ctx.set_source_rgb(0, 0, 0)
        ctx.stroke()


class NavigationToolBar2GTK3(NavigationToolbar2GTK3_old):
    toolitems = [t for t in NavigationToolbar2GTK3_old.toolitems if t[0] != 'Subplots']

    def __init__(self, canvas: 'FigureCanvasGTK3Agg'):
        super().__init__(canvas)

    def set_window(self, window: Gtk.Window):
        """ Sets the parent window of the widget, used to display the save plot dialog box
        """
        self.win = window

    def draw_rubberband(self, event, x0, y0, x1, y1):
        self.canvas.rubberband.update(x0, y0, x1, y1)
        self.canvas.rubberband.visible = True
        self.canvas.draw()
        self.canvas.rubberband.draw_rubberband(self.canvas)

    def remove_rubberband(self):
        super().remove_rubberband()
        self.canvas.rubberband.visible = False

    def release_zoom(self, event):
        self.canvas.user_request_redraw = True
        super().release_zoom(event)

    def draw(self):
        self.canvas.user_request_redraw = True
        super().draw()

    def drag_pan(self, event):
        self.canvas.user_request_redraw = True
        super().drag_pan(event)

    def forward(self, *args):
        self.canvas.user_request_redraw = True
        super().forward(*args)

    def home(self, *args):
        self.canvas.user_request_redraw = True
        super().home(*args)

    def back(self, *args):
        self.canvas.user_request_redraw = True
        super().back(*args)


class NavigationToolBar2GTK3Threaded(NavigationToolBar2GTK3):
    def set_cursor(self, cursor):
        self.canvas.get_property("window").set_cursor(cursord[cursor])

    def drag_pan(self, event):
        self.canvas.navigate(super().drag_pan, (event,))

    def forward(self, *args):
        self.canvas.navigate(super().forward, args)

    def home(self, *args):
        self.canvas.navigate(super().home, args)

    def back(self, *args):
        self.canvas.navigate(super().back, args)


class FigureCanvasGTK3Agg(FigureCanvasGTK3, FigureCanvasAgg):
    def __init__(self, figure: Figure):
        self.figure: Optional[Figure] = None
        super().__init__(figure)
        self._last_allocation_area: Optional[Tuple[int, int]] = None
        self._pending_buffer: Optional[Tuple[bytes, Tuple[int, int]]] = None
        """ Next buffer to be drawn."""

        self._displayed_buffer: Optional[Tuple[bytes, Tuple[int, int]]]= None
        """ Stores the data of the GdkPixbuf drawn on the canvas. Workaround as for some reason,
        this data will be garbage collected if not stored while still being used by the GdkPixbuf.
        Chaches the content of the displayed buffer to prevent it to be garbage collected."""

        self._bg_cache: GdkPixbuf.Pixbuf = None
        """PixBuffer as drawn on the Canvas. Used to restore the canvas on Gtk request
        avoiding a full redraw of the plot while it has not changed."""

        self.user_request_redraw = True
        """ Redraw on user request"""

        self._draw_resize_timestamp = time.time()
        """ Timestamp of the last draw request after a resize event. Used to avoid redrawing multiple time the
        plot when many requests are issued."""

        self._resize_timeout_cb_id = None
        """ Timeout callback id"""

        self._resize_event_restart = False
        """ True if a new resize event has occurred while a timeout was pending"""

        self._resize_redraw = False
        """Redraw after a resizing of the plot"""

        self.plot_without_overlay = None
        """ Plot without the overlay elements added for the user interaction."""

        self.overlay_artists: List[Artist] = []
        """ List of the artists drawn over the raw_plot_cache for user interaction."""

        self._draw_resize_delay = 0.1
        """ Delay in seconds between the moment when the request to draw is issued and the draw is performed.
        Use to filter out repetitive redraw requests, only the last is kept."""

        self._full_size = False
        """ If True, request a size equals to the size of the figure to render else,
        no size is requested. In the latter case, the figure will be shrinked to the allocated area"""

        self.rubberband = Rubberband()
        """ If visible, draws a selection rectangle other the plot."""

    def _renderer_init(self):
        pass

    def set_cursor(self, cursor):
        window = self.get_property("window")
        if window is not None:
            window.set_cursor(cursord[cursor])

    def render_figure(self):
        self.emit(START_RENDERING_FIGURE_SIGNAL, self.user_request_redraw)
        try:
            self.draw_plot_without_overlay()
            self.emit(START_RENDERING_OVERLAY_SIGNAL, self.user_request_redraw)
            self.set_overlay_artists_visible(True)
            self.draw_overlay_artists()
            self.update_plot_buffer()
        except Exception:
            logger.exception('Rendering failed')
        finally:
            self.emit(END_RENDERING_FIGURE_SIGNAL, self.user_request_redraw)

    def draw_plot_without_overlay(self):
        self.set_overlay_artists_visible(False)
        FigureCanvasAgg.draw(self)
        self.cache_plot_without_overlay()

    def cache_plot_without_overlay(self):
        self.plot_without_overlay = self.copy_from_bbox(self.figure.bbox)

    def restore_plot_without_overlay(self):
        if self.plot_without_overlay is not None:
            self.restore_region(self.plot_without_overlay)

    def set_overlay_artists_visible(self, visible):
        for artist in self.overlay_artists:
            artist.set_visible(visible)

    def show_full_size(self, full_size: bool):
        """

        :param bool full_size: True to display the plot at its full size
        :return:
        """
        self._full_size = full_size
        self.update_size_request()

    def update_size_request(self):
        if self._full_size:
            dpi = self.figure.get_dpi()
            width, height = self.figure.get_figwidth() * dpi, self.figure.get_figheight() * dpi
        else:
            width, height = -1, -1
        self.set_size_request(width, height)

    def add_overlay_artist(self, artist: Artist):
        """ Adds an artist to the overlay list.
        Warning: Adding several time the same artist instance can lead to unpredictable behaviour.

        :param matplotlib.artist.Artist artist:
        :return:
        """
        self.overlay_artists.append(artist)

    def remove_overlay_artist(self, artist: Artist):
        """ Removes an artist from the overlay list

        :param matplotlib.artist.Artist artist:
        :return:
        """
        self.overlay_artists.remove(artist)
        artist.remove()

    def draw_overlay_artists(self):
        if len(self.overlay_artists) > 0:
            for artist in self.overlay_artists:
                artist.draw(self.renderer)

    def redraw_overlay_artists(self):
        """ Redraws the overlay and updates the canvas.

        :return:
        """
        self.restore_plot_without_overlay()
        self.draw_overlay_artists()
        self.update_plot_buffer()
        self.draw()

    @staticmethod
    def convert_buffer_rgba(buffer_size):
        buffer_rgba, (width, height) = buffer_size
        image_rgb = np.frombuffer(buffer_rgba, np.uint8).reshape((height, width, 4))[:, :, 0:3]
        buffer_rgb = image_rgb.tobytes()
        return buffer_rgb, (width, height)

    @staticmethod
    def allocation_to_width_height(allocation):
        return allocation.width, allocation.height

    def draw_force(self):
        self.user_request_redraw = True
        self.draw()

    def draw_idle(self):
        super().draw_idle()

    def _resize_redraw_cb(self):
        # Checks whether a new resize event has occurs, in that case, delay the timeout.
        if self._resize_event_restart:
            self._resize_event_restart = False
            return True
        self._resize_timeout_cb_id = None
        self.draw()
        return False

    def get_need_redraw(self):
        return self.user_request_redraw

    def on_draw_event(self, widget, ctx):
        allocation_area = FigureCanvasGTK3Agg.allocation_to_width_height(self.get_allocation())

        # If the allocation area has changed, initialize the redrawing timeout
        if self._last_allocation_area is not None and self._last_allocation_area != allocation_area:
            self._draw_resize_timestamp = time.time()
            self._resize_redraw = True

        # If the plot must be redrawn, determined on these conditions:
        # - Plot was never drawn
        # - Redraw is forced by user
        # - Resize timeout has expired
        # then figure is rendered in the pending plot buffer.
        if self._last_allocation_area is None or self.get_need_redraw() or\
                self._resize_redraw and time.time() - self._draw_resize_timestamp > self._draw_resize_delay:
            self.render_figure()
            self._resize_redraw = False
            self.user_request_redraw = False
        # If a resize timeout is still waiting, delay the redraw. The goal is to ensure a good response time
        # of the gui by not rendering the plot each time.
        elif self._resize_redraw:
            # If a resize timeout is pending, flags a new resize event. So that the redraw is delayed for
            # another 100ms.
            if self._resize_timeout_cb_id is not None:
                self._resize_event_restart = True
            # Otherwise initializes a new timeout callback.
            else:
                self._resize_event_restart = False
                self._resize_timeout_cb_id = GLib.timeout_add(100, self._resize_redraw_cb)

        self._last_allocation_area = allocation_area

        # Updates the Gui canvas
        # If a new plot buffer is pending, use it to draw on the canvas widget.
        if self._pending_buffer is not None:
            self._draw_pending_buffer(allocation_area, ctx)
        # Otherwise, uses the last available cache of the plot if any, stretches is if required
        else:
            self._draw_resized_plot(allocation_area, ctx)

        self.rubberband.draw_rubberband(self, ctx)
        return False

    def _draw_pending_buffer(self, allocation_area, ctx):
        if self._pending_buffer is not None:
            buffer_rgb, (width, height) = self._pending_buffer
            pixbuf = GdkPixbuf.Pixbuf.new_from_data(buffer_rgb, GdkPixbuf.Colorspace.RGB, False, 8,
                                                    width, height, 3 * width, None, None)
            self._bg_cache = pixbuf
            self._draw_resized_plot(allocation_area, ctx)
            self._displayed_buffer = self._pending_buffer  # Do NOT remove, see self._displayed_buffer declaration.
            self._pending_buffer = None

    def _draw_resized_plot(self, allocation_area, ctx):
        if self._bg_cache is None:
            return

        width, height = allocation_area
        if width != self._bg_cache.get_width() or height != self._bg_cache.get_height():
            buffer = self._bg_cache.scale_simple(width, height, GdkPixbuf.InterpType.NEAREST)
        else:
            buffer = self._bg_cache
        self._draw_on_cairo_context(buffer, allocation_area, ctx)

    def _draw_on_cairo_context(self, pixbuf, allocation_area, ctx):
        Gdk.cairo_set_source_pixbuf(ctx, pixbuf, 0, 0)
        ctx.rectangle(0, 0, allocation_area[0], allocation_area[1])
        ctx.fill()

    def render_to_buffer(self):
        renderer = self.get_renderer()
        original_dpi = renderer.dpi
        renderer.dpi = self.figure.dpi
        try:
            result = (renderer.buffer_rgba(),
                      (int(renderer.width), int(renderer.height)))
        finally:
            renderer.dpi = original_dpi
        return self.convert_buffer_rgba(result)

    def update_plot_buffer(self):
        """
        Updates the plot buffer after adding artists to the figure.
        The changes will be updated to the gui the next time it is drawn.
        Call self.queue_draw or self.draw to update the gui.
        """
        self._pending_buffer = self.render_to_buffer()

    def size_allocate(self, widget, allocation):
        if not self._full_size:
            super().size_allocate(widget, allocation)

    def configure_event(self, widget, event):
        if not self._full_size:
            return super().configure_event(widget, event)
        return False


GObject.type_register(FigureCanvasGTK3Agg)
GObject.signal_new(START_RENDERING_FIGURE_SIGNAL, FigureCanvasGTK3Agg, GObject.SignalFlags.RUN_LAST,
                   GObject.TYPE_NONE, (GObject.TYPE_BOOLEAN,))
GObject.signal_new(START_RENDERING_OVERLAY_SIGNAL, FigureCanvasGTK3Agg, GObject.SignalFlags.RUN_LAST,
                   GObject.TYPE_NONE, (GObject.TYPE_BOOLEAN,))
GObject.signal_new(END_RENDERING_FIGURE_SIGNAL, FigureCanvasGTK3Agg, GObject.SignalFlags.RUN_LAST,
                   GObject.TYPE_NONE, (GObject.TYPE_BOOLEAN,))


T_Action_Function = Callable[[Any], None]


class FigureCanvasGTK3AggThread(FigureCanvasGTK3Agg):
    """
    Same as FigureCanvasGTK3Agg but the rendering is done in a separate thread.
    """

    def __init__(self, figure):
        super().__init__(figure)
        self.working_thread = None

        # List of functions to call before rendering the plot.
        self._pre_render_actions: List[Tuple[T_Action_Function, Tuple, Dict]] = []

        self._redraw_overlay_artists = False
        self._worker_buffer = None
        self._worker_pre_render_actions: List[T_Action_Function] = []

        self._start_allocation_area: Optional[Tuple[int, int]] = None
        self._pending_allocation = None

        # During rendering the size might not be the final size.
        # If idle_size is false, change in size allocation are ignored.
        self.idle_size = True

        # Navigation actions (using a NavigationToolBar) can occur at any moment,
        # even during the plot rendering, which can cause duplication of plots,
        # i.e. several plots overlapping when only on should be displayed, or
        # worst, segmentation faults. This attributes stores the navigation action
        # until the plot rendering is done to apply it afterward.
        self._pending_navigation = None

    def get_need_redraw(self):
        return super().get_need_redraw() or self._redraw_overlay_artists

    def render_figure(self):
        if self.get_idle_state():
            if self._redraw_overlay_artists and not self.user_request_redraw:
                self._redraw_overlay_artists = False
                overlay_only = True
            else:
                overlay_only = False
            self.user_request_redraw = False

            self._start_allocation_area = FigureCanvasGTK3Agg.allocation_to_width_height(self.get_allocation())
            self.emit(START_RENDERING_FIGURE_SIGNAL, self.user_request_redraw)
            pre_render_actions = self._pre_render_actions
            self._pre_render_actions = []
            self.render_thread_start(pre_render_actions, overlay_only)

    def thread_emit(self, signal, *args):
        GLib.idle_add(self._emit, signal, args)

    def _emit(self, signal, args):
        super().emit(signal, *args)

    def reset_pre_render_actions_queue(self):
        """ Used to clear the prerendering actions queue if they are not meant to be applied anymore.
        Usefull if the rendering of the plot took so long that actual queue is not relevant anymore.
        """
        self._pre_render_actions = []

    def add_pre_render_action(self, action: T_Action_Function, *args, **kwargs):
        """ Modifying the plot while the rendering thread is runnning can generate unexpected behaviour.
        For this reason, add here any call to functions that modify the figure. These function will be called just
        before the rendering.
        :param action: the function to call
        :param args: argument to pass to the action
        :param kwargs: keyword arguments to pass to the action
        """
        self._pre_render_actions.append((action, args, kwargs))

    def redraw_overlay_artists(self):
        self._redraw_overlay_artists = True
        self.draw()

    def render_thread_start(self, pre_render_actions: List[Tuple[T_Action_Function, Tuple, Dict]], overlay_only=False):
        logger.debug('Starting rendering thread')
        self.working_thread = Thread(target=self.render_thread, args=(pre_render_actions, overlay_only))
        self.working_thread.start()

    def render_thread(self, pre_render_actions: List[T_Action_Function], overlay_only=False):
        logger.debug('Rendering thread started')
        try:
            for cb, args, kwargs in pre_render_actions:
                cb(*args, **kwargs)
            if overlay_only:
                self.restore_plot_without_overlay()
            else:
                self.draw_plot_without_overlay()
            self.thread_emit(START_RENDERING_OVERLAY_SIGNAL, self.user_request_redraw)
            self.set_overlay_artists_visible(True)
            self.draw_overlay_artists()
            self._worker_buffer = self.render_to_buffer()
        except Exception:
            logger.exception('Rendering failed')
        finally:
            GLib.idle_add(self.render_thread_end, priority=GLib.PRIORITY_HIGH_IDLE)
        logger.debug('Rendering thread done')

    def render_thread_end(self):
        self.working_thread.join()
        self.working_thread = None
        logger.debug('Rendering thread joined')
        self.emit(END_RENDERING_FIGURE_SIGNAL, self.user_request_redraw)
        self._pending_buffer = self._worker_buffer
        self._last_allocation_area = self._start_allocation_area

        # During the rendering, GUI might displays some waiting animation,
        # which could change the allocation size. When END_RENDERING_FIGURE_SIGNAL is emitted,
        # we can assume that the allocation size has been restored. Not ignoring this would
        # lead to an infinite loop.
        if self._start_allocation_area == FigureCanvasGTK3Agg.allocation_to_width_height(self.get_allocation()):
            self._pending_allocation = None  # Drops any pending allocation to avoid infinite loop.
            self.queue_draw()
        elif self._pending_allocation is not None:
            widget, allocation = self._pending_allocation
            self._pending_allocation = None
            if FigureCanvasGTK3Agg.allocation_to_width_height(allocation) == self._pending_buffer[1]:
                logger.warning('Allocation and buffer have the same size, should we really render the plot again?')
            # This queues a new rendering tasks with the new allocation.
            self.size_allocate(widget, allocation)
        else:
            self.queue_draw()

    def get_idle_state(self):
        return self.working_thread is None and self.idle_size

    def size_allocate(self, widget, allocation):
        if self.get_idle_state():
            super().size_allocate(widget, allocation)
        else:
            self._pending_allocation = widget, allocation

    def navigate(self, method, args):
        if self.get_idle_state():
            self._pending_navigation = None
            method(*args)
        else:
            self._pending_navigation = (method, args)
            GLib.idle_add(self.apply_navigation)

    def apply_navigation(self):
        if self._pending_navigation is None:
            return False
        # Applies the pending navigation if state is idle
        if self.get_idle_state():
            method, args = self._pending_navigation
            self._pending_navigation = None
            method(*args)
            return False
        # Otherwise, keep the idle callback for next iteration
        else:
            return True

    def configure_event(self, widget, event):
        if self.get_idle_state():
            return super().configure_event(widget, event)
        return False


class FigureCanvasContainer(Gtk.Box):
    """
    A container to simplify usage of FigureCanvasGTK3AggThread by adding user feedback on the current
    rendering process.
    """

    def __init__(self, figure: Figure, waiting_overlay: Gtk.Widget = None, waiting_child: Gtk.Widget = None):
        """

        :param matplotlib.figure.Figure figure:
        :param Gtk.Widget waiting_overlay: A widget to display over the plot while rendering
        :param Gtk.Widget waiting_child: A widget to display at the bottom of the plot while rendering
        """
        super().__init__(orientation=Gtk.Orientation.VERTICAL, homogeneous=False)
        self.canvas = FigureCanvasGTK3AggThread(figure)
        self.canvas.show()

        self.overlay = Gtk.Overlay()
        self.overlay.add(self.canvas)
        self.pack_start(self.overlay, True, True, 0)

        self.waiting_overlay = waiting_overlay
        if self.waiting_overlay is not None:
            self.waiting_overlay.hide()
            self.overlay.add_overlay(self.waiting_overlay)

        self.waiting_child = waiting_child
        self.waiting_event_box = Gtk.EventBox()
        if self.waiting_child is not None:
            self.waiting_child.show()
            self.waiting_event_box.add(self.waiting_child)
            self.waiting_event_box.add_events(Gdk.EventMask.STRUCTURE_MASK)
            self.pack_start(self.waiting_event_box, False, False, 0)
            self.waiting_event_box.connect('unmap-event', self.on_waiting_event_box_unmap_event)

        self.overlay.show()
        self.canvas.connect(START_RENDERING_FIGURE_SIGNAL, self.on_start_rendering)
        self.canvas.connect(END_RENDERING_FIGURE_SIGNAL, self.on_end_rendering)

    def show_waiting_overlay(self):
        if self.waiting_overlay is not None:
            self.waiting_overlay.show()

    def hide_waiting_overlay(self):
        if self.waiting_overlay is not None:
            self.waiting_overlay.hide()

    def show_waiting_child(self):
        if self.waiting_child is not None:
            self.waiting_event_box.show()
            self.canvas.idle_size = False

    def hide_waiting_child(self):
        if self.waiting_child is not None:
            self.waiting_event_box.hide()

    def on_waiting_event_box_unmap_event(self, widget, event, data=None):
        self.canvas.idle_size = True
        return True

    def on_start_rendering(self, widget, user_request, data=None):
        self.show_waiting_overlay()
        self.show_waiting_child()

    def on_end_rendering(self, widget, user_request, data=None):
        self.hide_waiting_overlay()
        self.hide_waiting_child()


if __name__ == "__main__":
    from matplotlib.figure import Figure
    from mpl_gtk3_backend.overlays import CursorAnnotator, AnnotationParameters
    threaded = True

    handler = logging.StreamHandler()
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    handler.setLevel(logging.DEBUG)

    class SampleAnnotator(CursorAnnotator):
        def make_annotation(self, pick_event):
            line = pick_event.artist
            ":type: matplotlib.line.Line2D"
            indexes = pick_event.ind
            x_out_lst, y_out_lst = (line.get_xdata()[indexes], line.get_ydata()[indexes])
            closest = np.argmin(np.abs(x_out_lst - pick_event.mouseevent.xdata))
            x_out, y_out = x_out_lst[closest], y_out_lst[closest]
            text = '{}\n{}'.format(x_out, y_out)
            parameters = {'bbox': {'edgecolor': line.get_color(),
                                   'boxstyle': 'round', 'fc': '0.8'},
                          'horizontalalignment': 'center'}
            arrowprops = {'facecolor': line.get_color()}
            return AnnotationParameters(text, (x_out, y_out), textkwargs=parameters, arrowprops=arrowprops)

    def prepare_plot(figure: Figure):
        plot = figure.add_subplot(121)
        x = np.linspace(0, 10*np.pi, 10000)
        for i in range(100):
            plot.plot(x, np.sin(x + np.divide(i, np.pi)))

        plot2 = figure.add_subplot(122)
        lines = plot2.plot(x, np. sin(x), picker=True)

    window = Gtk.Window()

    figure = Figure()
    if threaded:
        spinner = Gtk.Spinner()
        spinner.connect('show', lambda w: w.start())
        spinner.connect('hide', lambda w: w.stop())

        canvas_container = FigureCanvasContainer(figure, spinner)
        canvas_container.show()
        canvas = canvas_container.canvas
        canvas_container.canvas.add_pre_render_action(lambda: prepare_plot(figure))
        navigation = NavigationToolBar2GTK3Threaded(canvas)
    else:
        canvas = FigureCanvasGTK3Agg(figure)
        navigation = NavigationToolBar2GTK3(canvas)
        canvas_container = canvas
        canvas.show()
        prepare_plot(figure)

    annotator = SampleAnnotator(canvas)

    window.set_size_request(800, 600)
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    window.show()
    plot_container = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    plot_container.show()
    plot_container.pack_start(canvas_container, True, True, 0)
    plot_container.pack_start(navigation, False, False, 2)
    window.add(plot_container)

    GLib.idle_add(lambda: canvas.draw())
    Gtk.main()
